﻿using UnityEngine;
using System.Collections;

public class Frank : MonoBehaviour
{
    public float Mass;
    public Vector2 Velocity;
    public bool Collided;
    public bool Gravity;
    public CharacterController Controller;

    public Rect BoundingBox;
    public Vector2 Acceleration;
    public Vector2 gravity;
    float MaxVelocity = 0.8f;
    float MinVelocity = -0.8f;


    // Use this for initialization
    void Start()
    {
        BoundingBox.size = transform.localScale;
        BoundingBox.center = transform.position;
        
        gravity = new Vector2(0, -0.1f * Mass);
        if (gameObject.tag == "Ball")
        {
            Renderer rend = GetComponent<Renderer>();
            rend.material.color = Color.red;
        }
    }

    // Update for physics computations
    void FixedUpdate()
    {
        //BoundingBox.position = new Vector2(BoundingBox.xMin, BoundingBox.yMin);

        Accelerate();

        //Gets all objects that are tagged Static , Bouncy respectively
        GameObject[] StaticObj = GameObject.FindGameObjectsWithTag("Static");
        GameObject[] BounceObj = GameObject.FindGameObjectsWithTag("Bouncy");
        GameObject[] BreakObj = GameObject.FindGameObjectsWithTag("Breakable");
        GameObject[] Ball = GameObject.FindGameObjectsWithTag("Ball");
        GameObject[] Player = GameObject.FindGameObjectsWithTag("Player");

        //Sets velocity for each kind of collision
        CollisionChecker(StaticObj, -Velocity);
        CollisionChecker(BounceObj, -Velocity * 2);
        CollisionChecker(BreakObj, -Velocity * 2);
        CollisionChecker(Ball, Velocity);
        CollisionChecker(Player, -Velocity * 2.5f);
    }
    // Update is called once per frame
    void Update()
    {
        DrawCollider();
    }

    /// <summary>   
    /// Draws the Box Collider
    /// </summary>
    void DrawCollider()
    {
        //print(BoundingBox.width);
        Vector2 UpperLeft = new Vector2(BoundingBox.xMin, BoundingBox.yMin);
        Vector2 UpperRight = new Vector2(BoundingBox.xMax, BoundingBox.yMin);
        Vector2 LowerLeft = new Vector2(BoundingBox.xMin, BoundingBox.yMax);
        Vector2 LowerRight = new Vector2(BoundingBox.xMax, BoundingBox.yMax);

        Debug.DrawLine(UpperLeft, UpperRight, Color.red);
        Debug.DrawLine(UpperLeft, LowerLeft, Color.red);
        Debug.DrawLine(LowerLeft, LowerRight, Color.red);
        Debug.DrawLine(LowerRight, UpperRight, Color.red);
    }

    // Collision Detection AABB
    public static bool Collide(Rect a, Rect b)
    {
        float d1x = b.xMin - a.xMax;
        float d1y = b.yMin - a.yMax;
        float d2x = a.xMin - b.xMax;
        float d2y = a.yMin - b.yMax;

        if (d1x > 0.0f || d1y > 0.0f)
            return false;
        if (d2x > 0.0f || d2y > 0.0f)
            return false;

        return true;
    }

    public void Accelerate()
    {
        BoundingBox.position += Velocity; //reverted back because of bugs
        //BoundingBox.position = new Vector2(transform.position.x - (transform.localScale.x / 2), transform.position.y - (transform.localScale.y / 2));
        //BoundingBox.position = transform.position - (transform.localScale / 2);
        //BoundingBox.center = transform.position;
        transform.position += ConvertToVector3(Velocity);

        Velocity += Acceleration;

        if (Gravity) { Acceleration += gravity; }
        ClampVelocity();
        

    }

    void CollisionChecker(GameObject[] objects, Vector2 velocity)
    {
        foreach (GameObject obs in objects)
        {
            if (this.BoundingBox != obs.GetComponent<Frank>().BoundingBox) //checker so that obj doesn't collide with it's own bounding box
            {
                if (Collide(BoundingBox, obs.GetComponent<Frank>().BoundingBox))
                {
                    
                    //ResolveCollision(gameObject, obs); //not working as intended. instead, revert back to:
                    Collided = true;
                    Velocity += velocity;

                    if (gameObject.tag == "Breakable")
                    {
                        Destroy(gameObject, 0.5f);
                    }
                    else if (gameObject.tag == "Ball")
                    {
                        //float parallel = DotProduct(obs.transform.localPosition, this.BoundingBox.position);
                        //Debug.Log(parallel);
                        //if (parallel >= 1 || parallel <= -1)                          //not working as intended
                        //{
                            this.Velocity.x += Random.Range(-0.07f, 0.07f);
                        //}
                    }

                }
                else
                {
                    Collided = false;
                }
            }
        }
    }

    /// <summary>
    ///  Adds the given Vector2 force to the object's acceleration
    /// </summary>
    /// <param name="force"></param>
    //void AddForce(Vector2 force)
    //{
    //    Acceleration += force;            // with AddVector(Acceleration, force) this code is kind of redundant

    //}


    // Math Functions
    public Vector2 AddVector(Vector2 vector_1, Vector2 vector_2)
    {
        Vector2 sum = new Vector2(vector_1.x + vector_2.x, vector_2.x + vector_2.y);
        return sum;
    }
    public Vector2 SubtractVector(Vector2 vector_1, Vector2 vector_2)
    {
        return vector_1 - vector_2;
    }
    public Vector2 MultiplyVector(Vector2 vector, float num)
    {
        return vector * num;
    }
    public Vector2 DivideVector(Vector2 vector, float num)
    {
        Vector2 quotient = new Vector2(vector.x / num, vector.y / num);
        return quotient;
    }
    public float DotProduct(Vector2 vector_1, Vector2 vector_2)
    {
        return (vector_1.x * vector_2.x) + (vector_1.y * vector_2.y);
    }
    public float Min(float A, float B)
    {
        if (A > B) { return B; }
        else { return A; }
    }
    public Vector2 NormalizeVector(Vector2 vector)
    {
        float x = vector.x / vector.magnitude;
        float y = vector.y / vector.magnitude;

        return new Vector2(x, y);
    }
    public Vector3 ConvertToVector3(Vector2 vector)
    {
        Vector3 Vector = new Vector3(vector.x, vector.y, 0);
        return Vector;
    }

    //angular rotation
    
    void AnglularRotate(float angle)
    {
        float aVelocity = 0;
        float aAcceleration = 0.01f;
        
        angle += aVelocity;
        aVelocity += aAcceleration;
    }

    public void ClampVelocity()
    {
        if (Velocity.x >= MaxVelocity)
        {
            Velocity.x = MaxVelocity;
        }
        else if (Velocity.x <= MinVelocity)
        {
            Velocity.x = MinVelocity;
        }
        if (Velocity.y >= MaxVelocity)
        {
            Velocity.y = MaxVelocity;
        }
        else if (Velocity.y <= MinVelocity)
        {
            Velocity.y = MinVelocity;
        }
    }
}
